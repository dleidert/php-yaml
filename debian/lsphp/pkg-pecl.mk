#!/usr/bin/make -f
# See debhelper(7) (uncomment to enable)
# output every command that modifies files on the build system.
#DH_VERBOSE = 1

define NEWLINE

endef

ifeq ($(DH_VERBOSE), 1)
export SHELL = /bin/bash -ex
else
export SHELL = /bin/bash -e
endif

# see EXAMPLES in dpkg-buildflags(1) and read /usr/share/dpkg/*
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/default.mk

# see FEATURE AREAS in dpkg-buildflags(1)
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# see ENVIRONMENT in dpkg-buildflags(1)
# package maintainers to append CFLAGS
export DEB_CFLAGS_MAINT_APPEND  = -Wall -pedantic
# package maintainers to append LDFLAGS
export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

# Don't ever use RPATH on Debian
export PHP_RPATH=no

# Tests should run without interaction
export NO_INTERACTION=1

ifdef LSPHP_VERSIONS
PHP_VERSIONS := $(LSPHP_VERSIONS)
else
PHP_VERSIONS := $(shell /usr/sbin/phpquery -V)
endif

PECL_NAME    := $(if $(PECL_NAME_OVERRIDE),$(PECL_NAME_OVERRIDE),$(subst php-,,$(DEB_SOURCE)))
INSTALL_ROOT  = $(CURDIR)/debian/lsphp$(subst .,,$(*))-$(PECL_NAME)
SOURCE_DIR    = build-$(*)
LSPHP_VER     = $(subst .,,$(*))
CONFIG_DIR    = $(shell /usr/local/lsws/lsphp$(LSPHP_VER)/bin/php -r "echo PHP_CONFIG_FILE_SCAN_DIR;")
INI_DIR       = /usr/local/lsws/lsphp$(LSPHP_VER)/share/lsphp$(LSPHP_VER)-$(PECL_NAME)/$(PECL_NAME)

# find corresponding package-PHP_MAJOR.PHP_MINOR.xml, package-PHP_MAJOR.xml or package.xml
$(foreach ver,$(PHP_VERSIONS),$(eval PACKAGE_XML_$(ver) := $(word 1,$(wildcard package-$(ver).xml package-$(basename $(ver)).xml package.xml))))
# fill DH_PHP_VERSIONS with versions that have corresponding package.xml and are not outside the specification
$(foreach ver,$(PHP_VERSIONS),$(eval PHP_MIN_VER_$(ver) := $(if $(PACKAGE_XML_$(ver)),$(shell xml2 < $(PACKAGE_XML_$(ver)) | sed -ne "s,^/package/dependencies/required/php/min=\([0-9]\+\.[0-9]\+\).*,\1,p"),)))
$(foreach ver,$(PHP_VERSIONS),$(eval PHP_MAX_VER_$(ver) := $(if $(PACKAGE_XML_$(ver)),$(shell xml2 < $(PACKAGE_XML_$(ver)) | sed -ne "s,^/package/dependencies/required/php/max=\([0-9]\+\.[0-9]\+\).*,\1,p"),)))
MIN_PHP_VERSIONS := $(foreach ver,$(PHP_VERSIONS),$(if $(PHP_MIN_VER_$(ver)),$(shell dpkg --compare-versions "$(PHP_MIN_VER_$(ver))" le "$(ver)" && echo "$(ver)"),$(ver)))
MAX_PHP_VERSIONS := $(foreach ver,$(MIN_PHP_VERSIONS),$(if $(PHP_MAX_VER_$(ver)),$(shell dpkg --compare-versions "$(PHP_MAX_VER_$(ver))" gt "$(ver)" && echo "$(ver)"),$(ver)))
export DH_PHP_VERSIONS = $(if $(DH_PHP_VERSIONS_OVERRIDE),$(DH_PHP_VERSIONS_OVERRIDE),$(foreach ver,$(MAX_PHP_VERSIONS),$(if $(PACKAGE_XML_$(ver)),$(ver))))

ifndef SHORT_DESCRIPTION
SHORT_DESCRIPTION = $(shell xml2 < $(word 1,$(wildcard package-$(*).xml package-$(basename $(*)).xml package.xml)) | grep "^/package/summary=" | sed -ne "s,^/package/summary=,,g")
endif
ifndef LONG_DESCRIPTION
LONG_DESCRIPTION  = $(shell xml2 < $(word 1,$(wildcard package-$(*).xml package-$(basename $(*)).xml package.xml)) | grep "^/package/description=" | sed -ne "s,^/package/description=, ,g" -e "s,$,\n,g")
endif
export SHORT_DESCRIPTION
export LONG_DESCRIPTION

# for each ver in $(DH_PHP_VERSIONS), look into each corresponding package.xml for upstream PECL version
$(foreach ver,$(DH_PHP_VERSIONS),$(eval PECL_SOURCE_$(ver) := $(if $(PACKAGE_XML_$(ver)),$(shell xml2 < $(PACKAGE_XML_$(ver)) | sed -ne "s,^/package/name=,,p")-$(shell xml2 < $(PACKAGE_XML_$(ver)) | sed -ne "s,^/package/version/release=,,p"),undefined)))

CONFIGURE_TARGETS  = $(addprefix configure-,$(addsuffix -stamp,$(DH_PHP_VERSIONS)))
BUILD_TARGETS      = $(addprefix build-,$(addsuffix -stamp,$(DH_PHP_VERSIONS)))
INSTALL_TARGETS    = $(addprefix install-,$(addsuffix -stamp,$(DH_PHP_VERSIONS)))
TEST_TARGETS       = $(addprefix test-,$(addsuffix -stamp,$(DH_PHP_VERSIONS)))
CLEAN_TARGETS      = $(addprefix clean-,$(addsuffix -stamp,$(DH_PHP_VERSIONS)))
GENCONTROL_TARGETS = $(addprefix gencontrol-,$(addsuffix -stamp,$(DH_PHP_VERSIONS)))
LINTIAN_TARGETS    = $(addprefix debian/lsphp,$(addsuffix -$(PECL_NAME).lintian-overrides,$(subst .,,$(DH_PHP_VERSIONS))))
UCF_TARGETS        = $(addprefix debian/lsphp,$(addsuffix -$(PECL_NAME).ucf,$(subst .,,$(DH_PHP_VERSIONS))))
POSTINST_TARGETS   = $(addprefix debian/lsphp,$(addsuffix -$(PECL_NAME).postinst,$(subst .,,$(DH_PHP_VERSIONS))))
PRERM_TARGETS      = $(addprefix debian/lsphp,$(addsuffix -$(PECL_NAME).perm,$(subst .,,$(DH_PHP_VERSIONS))))

%:
	dh $@

override_dh_auto_configure: $(CONFIGURE_TARGETS)
override_dh_auto_build: $(BUILD_TARGETS)
override_dh_auto_install: $(INSTALL_TARGETS)
override_dh_auto_test: $(TEST_TARGETS)
override_dh_auto_clean: $(CLEAN_TARGETS)
	-$(RM) $(CONFIGURE_TARGETS) $(BUILD_TARGETS) $(INSTALL_TARGETS) $(TEST_TARGETS) $(CLEAN_TARGETS) $(GENCONTROL_TARGETS) $(LINTIAN_TARGETS) $(UCF_TARGETS) $(POSTINST_TARGETS) $(PRERM_TARGETS)
	-$(RM) debian/source/lintian-overrides

override_dh_gencontrol: $(GENCONTROL_TARGETS)
override_dh_lintian: $(LINTIAN_TARGETS)
	dh_lintian

override_dh_ucf: $(UCF_TARGETS)
	dh_ucf

override_dh_clean: debian/source/lintian-overrides
	dh_clean

clean-%-stamp:
	$(RM) -r $(SOURCE_DIR)
	touch clean-$(*)-stamp

configure-%-stamp:
	cp -a $(PECL_SOURCE_$(*)) $(SOURCE_DIR)
	cd $(SOURCE_DIR) && /usr/local/lsws/lsphp$(LSPHP_VER)/bin/phpize
	dh_auto_configure --sourcedirectory=$(SOURCE_DIR) -- --enable-$(PECL_NAME) --with-php-config=/usr/local/lsws/lsphp$(LSPHP_VER)/bin/php-config $(PECL_CONFIGURE_MAINT_APPEND)
	touch configure-$(*)-stamp

build-%-stamp:
	dh_auto_build --sourcedirectory=$(SOURCE_DIR)
	touch build-$(*)-stamp

install-%-stamp:
	dh_auto_install --sourcedirectory=$(SOURCE_DIR) -- INSTALL_ROOT=$(INSTALL_ROOT)
	mkdir -p $(INSTALL_ROOT)/$(INI_DIR)
	cp $(CURDIR)/debian/$(PECL_NAME).ini $(INSTALL_ROOT)/$(INI_DIR)/
	sed -e "s/@@PECL_NAME@@/$(PECL_NAME)/g" -e "s/@@LSPHP_VERSION@@/$(*)/g" $(CURDIR)/debian/lsphp/binary/postinst.in > $(INSTALL_ROOT).postinst
	sed -e "s/@@PECL_NAME@@/$(PECL_NAME)/g" -e "s/@@LSPHP_VERSION@@/$(*)/g" $(CURDIR)/debian/lsphp/binary/prerm.in    > $(INSTALL_ROOT).prerm
	touch install-$(*)-stamp

test-%-stamp:
	dh_auto_test --sourcedirectory=$(SOURCE_DIR) -- INSTALL_ROOT=$(INSTALL_ROOT)
	touch test-$(*)-stamp

gencontrol-%-stamp:
	echo "$$LONG_DESCRIPTION" | xargs -0 -I% \
	    dh_gencontrol -plsphp$(LSPHP_VER)-$(PECL_NAME) -- -Vlsphp:Depends="lsphp$(LSPHP_VER), lsphp$(LSPHP_VER)-common" \
	                                                      -Vpecl:Short="$(SHORT_DESCRIPTION) extension module for PHP $(*)" \
	                                                      -Vpecl:Long="%"
	touch gencontrol-$(*)-stamp

debian/lsphp%-$(PECL_NAME).lintian-overrides: debian/lsphp/binary/lintian-overrides
	-cat $(@).in > $(@)
	cat $(<) >> $(@)

debian/source/lintian-overrides: debian/lsphp/source/lintian-overrides
	-cat $(@).in > $(@)
	cat $(<) >> $(@)

debian/lsphp%-$(PECL_NAME).ucf:
	$(shell echo "$(INI_DIR)/$(PECL_NAME).ini $(CONFIG_DIR)/$(PECL_NAME).ini" > $(@))

override_dh_php:
ifneq (,$(filter xenial,$(dist)))
	#touch $(CURDIR)/debian/lsphp-$(PECL_NAME).php
endif
	# do nothing

override_dh_usrlocal:
	# do nothing
